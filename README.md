Packaging for python library kalyke-apns in XiVO as a debian package

on pypi : https://pypi.org/project/kalyke-apns/  
on github : https://github.com/nnsnodnb/kalyke  

To build another version of kalyke-apns in the XiVO repository, set the
desired version in the VERSION file and increment the changelog.

Clean after local build :

git clean -id
git checkout -- .gitignore README.md